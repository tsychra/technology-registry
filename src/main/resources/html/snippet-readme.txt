To embed registry snippet to your web-page, follow these steps:
1) Copy [//JS_FILES//] somewhere to your web application
2) Put contents of head.html to <head> element your HTML file and optionally update paths to previously copied JS files
3) Put contents of registry.html anywhere to your HTML file