let randomSeed = 42; // answer to everything, right?

function visualizeRegistry(config, data) {
    let ctx = initContext(config, data);

    initSvg(ctx);
    positionTechnologiesRandomly(ctx);
    groupTechnologiesToSegments(ctx);
    drawGridLines(ctx);
    drawLifecycle(ctx);

    if (!config.compactMode) {
        drawTitle(ctx);
        drawFooter(ctx);
    }

    drawLegend(ctx);
    prepareBubble(ctx);
    drawTechnologies(ctx);
    doSimulation(ctx);
}

function initContext(config, data) {
    const circleRadius = 400;
    const legendColWidth = 120;
    const baseMargin = 50;

    const baseSize = (2 * circleRadius) + (4 * baseMargin);
    const width = config.compactMode ? baseSize : (baseSize + (4 * legendColWidth));
    const leftmostOffset = -1 * (circleRadius + (2 * legendColWidth) + baseMargin);
    const topmostOffset = -1 * (circleRadius + baseMargin);

    let legendOffset = [
        {x: circleRadius + baseMargin, y: 2 * baseMargin},
        {x: leftmostOffset, y: 2 * baseMargin},
        {x: leftmostOffset, y: -(circleRadius - (2 * baseMargin))},
        {x: circleRadius + baseMargin, y: -(circleRadius - (2 * baseMargin))}
    ];
    if (config.compactMode) {
        legendOffset = [
            {x: circleRadius / 1.5, y: circleRadius - baseMargin},
            {x: -(circleRadius + config.legend.category.fontSize), y: circleRadius - baseMargin},
            {x: -(circleRadius + config.legend.category.fontSize), y: -(circleRadius - baseMargin)},
            {x: circleRadius / 1.5, y: -(circleRadius - baseMargin)}
        ]
    }

    return {
        data: data,
        config: config,
        width: width,
        height: baseSize,
        circleRadius: circleRadius,
        circleLifecycle: config.lifecycle.filter(r => r.inCircle),
        categories: [
            // radial_min / radial_max are multiples of PI
            {radial_min: 0, radial_max: 0.5, factor_x: 1, factor_y: 1},
            {radial_min: 0.5, radial_max: 1, factor_x: -1, factor_y: 1},
            {radial_min: -1, radial_max: -0.5, factor_x: -1, factor_y: -1},
            {radial_min: -0.5, radial_max: 0, factor_x: 1, factor_y: -1}
        ],
        titleOffset: {x: leftmostOffset, y: topmostOffset},
        legendColWidth: legendColWidth,
        legendOffset: legendOffset,
        footerOffset: {x: leftmostOffset, y: -topmostOffset},
        paddingBase: 37
    };
}

function initSvg(ctx) {
    let svg = d3.select("svg#registry")
        .style("background-color", ctx.config.backgroundColor)
        .attr("width", ctx.width)
        .attr("height", ctx.height);

    ctx.svgGroup = svg.append("g");

    if (ctx.config.zoomedCategory !== -1) {
        svg.attr("viewBox", [
            Math.max(0, ctx.categories[ctx.config.zoomedCategory].factor_x * ctx.circleRadius) - 420,
            Math.max(0, ctx.categories[ctx.config.zoomedCategory].factor_y * ctx.circleRadius) - 420,
            ctx.circleRadius + 40,
            ctx.circleRadius + 40
        ].join(" "));
    } else {
        ctx.svgGroup.attr("transform", translate(ctx.width / 2, ctx.height / 2));
    }

    ctx.grid = ctx.svgGroup.append("g");
}

function drawGridLines(ctx) {
    ctx.grid.append("line")
        .attr("x1", 0).attr("y1", -ctx.circleRadius)
        .attr("x2", 0).attr("y2", ctx.circleRadius)
        .style("stroke", ctx.config.circleLinesColor)
        .style("stroke-width", 1);
    ctx.grid.append("line")
        .attr("x1", -ctx.circleRadius).attr("y1", 0)
        .attr("x2", ctx.circleRadius).attr("y2", 0)
        .style("stroke", ctx.config.circleLinesColor)
        .style("stroke-width", 1);
}

function drawLifecycle(ctx) {
    for (let i = 0; i < ctx.circleLifecycle.length; i++) {
        const lifecycleCfg = ctx.config.lifecycle[i];
        ctx.grid.append("circle")
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", ctx.circleLifecycle[i].radius)
            .style("fill", "none")
            .style("stroke", ctx.config.circleLinesColor)
            .style("stroke-width", 1);

        ctx.grid.append("text")
            .text(lifecycleCfg.name.toUpperCase())
            .attr("id", "lifecycle" + i)
            .attr("y", -ctx.circleLifecycle[i].radius + 30)
            .attr("text-anchor", "middle")
            .style("font-family", ctx.config.fontFamily)
            .style("font-size", lifecycleCfg.fontSize)
            .style("fill", lifecycleCfg.fontColor)
            .style("pointer-events", "none")
            .style("user-select", "none");

        const lifecycleText = d3.select("#lifecycle" + i).node();
        const textBox = lifecycleText.getBBox();
        ctx.grid.insert("rect", "text")
            .attr("x", textBox.x)
            .attr("y", textBox.y)
            .attr("width", textBox.width)
            .attr("height", textBox.height)
            .attr("fill", lifecycleCfg.fontBackgroundColor)

    }
}

function drawTitle(ctx) {
    ctx.svgGroup.append("text")
        .attr("transform", translate(ctx.titleOffset.x, ctx.titleOffset.y))
        .text(ctx.data.title)
        .style("font-family", ctx.config.fontFamily)
        .style("font-size", ctx.config.title.fontSize)
        .style("fill", ctx.config.title.fontColor);
}

function drawFooter(ctx) {
    ctx.svgGroup.append("text")
        .attr("transform", translate(ctx.footerOffset.x, ctx.footerOffset.y))
        .text("▲ promoted     ▼ demoted")
        .attr("xml:space", "preserve")
        .style("font-family", ctx.config.fontFamily)
        .style("font-size", ctx.config.footer.fontSize)
        .style("fill", ctx.config.footer.fontColor);
}

function drawLegend(ctx) {
    const categoriesCfg = ctx.config.categories;
    const lifecycleCfg = ctx.config.lifecycle;
    const legend = ctx.svgGroup.append("g");

    for (let category = 0; category < categoriesCfg.length; category++) {
        const categoryColors = categoriesCfg[category].technologyIconColors.length;
        const brightestCategoryColor = categoriesCfg[category].technologyIconColors[categoryColors - 1];

        let x = ctx.legendOffset[category].x;
        let y = ctx.legendOffset[category].y;
        if (!ctx.config.compactMode) y -= (ctx.config.legend.category.fontSize + ctx.config.legend.lifecycle.fontSize);

        legend.append("text")
            .attr("transform", translate(x, y))
            .text(categoriesCfg[category].name.toUpperCase())
            .style("font-family", ctx.config.fontFamily)
            .style("font-size", ctx.config.legend.category.fontSize)
            .style("fill", brightestCategoryColor);

        if (ctx.config.compactMode) {
            continue; // compact mode does not include legend of lifecycle(s), only a title of category
        }

        for (let lifecycle = 0; lifecycle < lifecycleCfg.length; lifecycle++) {
            legend.append("text")
                .attr("transform", positionTechnologyLegend(ctx, category, lifecycle, -1))
                .text(lifecycleCfg[lifecycle].name.toUpperCase())
                .style("font-family", ctx.config.fontFamily)
                .style("font-size", ctx.config.legend.lifecycle.fontSize)
                .style("fill", ctx.config.legend.lifecycle.fontColor);
            legend.selectAll(".legend" + category + lifecycle)
                .data(ctx.segmentedTechnologies[category][lifecycle])
                .enter()
                .append("a")
                .attr("xlink:href", function (d) {
                    return d.link;
                })
                .attr("target", "_blank")
                .append("text")
                .attr("class", "legend" + category + lifecycle)
                .attr("transform", function (d, i) {
                    return positionTechnologyLegend(ctx, category, lifecycle, i);
                })
                .text(function (d) {
                    if (d.moved > 0) return d.name + "▲ ";
                    if (d.moved < 0) return d.name + "▼ ";
                    return d.name;
                })
                .style("font-family", ctx.config.fontFamily)
                .style("font-size", ctx.config.legend.technology.fontSize)
                .style("fill", ctx.config.legend.technology.fontColor);
        }
    }
}

function drawTechnologies(ctx) {
    const circleTechnologies = ctx.data.technologies.filter(t => ctx.config.lifecycle.find(r => r.name === t.lifecycle).inCircle);
    const rink = ctx.svgGroup.append("g")
        .attr("id", "rink");
    ctx.visualizedTechnologies = rink.selectAll(".blip")
        .data(circleTechnologies)
        .enter()
        .append("g")
        .attr("class", "blip")
        .on("mouseover", showBubble)
        .on("mouseout", hideBubble);

    // configure each blip
    ctx.visualizedTechnologies.each(function (technology) {
        let blip = d3.select(this);
        const lifecycleCfg = ctx.config.lifecycle.find(r => r.name === technology.lifecycle);

        // technology link
        blip = blip.append("a")
            .attr("xlink:href", technology.link)
            .attr("target", "_blank");

        // technology shape
        if (technology.moved > 0) {
            blip.append("path")
                .attr("d", "M -10,5 10,5 0,-10 z") // triangle pointing up
                .style("fill", technology.color);
        } else if (technology.moved < 0) {
            blip.append("path")
                .attr("d", "M -10,-5 10,-5 0,10 z") // triangle pointing down
                .style("fill", technology.color);
        } else {
            blip.append("circle")
                .attr("r", lifecycleCfg.technologies.iconRadius)
                .attr("fill", technology.color);
        }

        // technology text
        blip.append("text")
            .text(technology.name)
            .attr("text-anchor", "middle")
            .style("font-family", ctx.config.fontFamily)
            .style("fill", lifecycleCfg.technologies.fontColor)
            .style("pointer-events", "none")
            .style("user-select", "none")
            .attr("y", lifecycleCfg.technologies.fontSize + (lifecycleCfg.technologies.iconRadius) + 2)
            .style("font-size", function () {
                let initialSize = lifecycleCfg.technologies.fontSize;
                if (technology.name.length >= 10) initialSize -= 1; // if name is long
                if (technology.name.length >= 15) initialSize -= 1; // if name is very long
                if (technology.name.length >= 20) initialSize -= 1; // if name is very very long
                if (technology.name.length >= 23) initialSize -= 1; // wtf dude
                return initialSize;
            });
    });
}

function doSimulation(ctx) {
    // make sure that blips stay inside their segment
    function ticked() {
        ctx.visualizedTechnologies.attr("transform", function (d) {
            if (d.segment === undefined) return translate(0, 0);
            return translate(d.segment.clipx(d), d.segment.clipy(d));
        })
    }

    d3.forceSimulation()
        .nodes(ctx.data.technologies)
        .velocityDecay(0.19) // magic number (found by experimentation)
        .force("collision", d3.forceCollide().radius(ctx.paddingBase).strength(0.7))
        .on("tick", ticked);
}

function prepareBubble(ctx) {
    const bubble = ctx.svgGroup.append("g")
        .attr("id", "bubble")
        .attr("x", 0)
        .attr("y", 0)
        .style("opacity", 0)
        .style("pointer-events", "none")
        .style("user-select", "none");

    bubble.append("rect")
        .attr("rx", ctx.config.bubble.cornersRadius)
        .attr("ry", ctx.config.bubble.cornersRadius)
        .style("fill", ctx.config.bubble.backgroundColor);

    bubble.append("text")
        .style("font-family", ctx.config.fontFamily)
        .style("font-size", ctx.config.bubble.fontSize)
        .style("fill", ctx.config.bubble.fontColor);

    bubble.append("path")
        .attr("d", "M 0,0 10,0 5,8 z")
        .style("fill", ctx.config.bubble.backgroundColor);
}

function positionTechnologiesRandomly(ctx) {
    const technologies = ctx.data.technologies;
    for (let i = 0; i < technologies.length; i++) {
        const technology = technologies[i];

        const techCategoryIdx = ctx.config.categories.map(q => q.name)
            .indexOf(technology.category);

        const techLifecycleIdx = ctx.config.lifecycle.filter(r => r.inCircle)
            .map(r => r.name)
            .indexOf(technology.lifecycle);
        if (techLifecycleIdx === -1) continue; // not in circle

        technology.segment = segment(ctx, techCategoryIdx, techLifecycleIdx);
        const point = technology.segment.random();

        technology.x = point.x;
        technology.y = point.y;
        technology.color = ctx.config.categories[techCategoryIdx].technologyIconColors[techLifecycleIdx];
    }
}

function segment(ctx, category, lifecycle) {
    const paddings = ctx.paddingBase;
    const polar_min = {
        t: ctx.categories[category].radial_min * Math.PI,
        r: (lifecycle === 0) ? 30 : ctx.circleLifecycle[lifecycle - 1].radius
    };
    const polar_max = {
        t: ctx.categories[category].radial_max * Math.PI,
        r: ctx.circleLifecycle[lifecycle].radius
    };
    const cartesian_min = {
        x: paddings * ctx.categories[category].factor_x,
        y: paddings * ctx.categories[category].factor_y
    };
    const cartesian_max = {
        x: ctx.circleLifecycle[ctx.circleLifecycle.length - 1].radius * ctx.categories[category].factor_x,
        y: ctx.circleLifecycle[ctx.circleLifecycle.length - 1].radius * ctx.categories[category].factor_y
    };
    return {
        clipx: function (d) {
            const c = bounded_box(d, cartesian_min, cartesian_max);
            const p = bounded_ring(polar(c), polar_min.r + paddings, polar_max.r - paddings);
            d.x = cartesian(p).x; // adjust data too!
            return d.x;
        },
        clipy: function (d) {
            const c = bounded_box(d, cartesian_min, cartesian_max);
            const p = bounded_ring(polar(c), polar_min.r + paddings, polar_max.r - paddings);
            d.y = cartesian(p).y; // adjust data too!
            return d.y;
        },
        random: function () {
            return cartesian({
                t: random_between(polar_min.t, polar_max.t),
                r: normal_between(polar_min.r, polar_max.r)
            });
        }
    }
}

function groupTechnologiesToSegments(ctx) {
    ctx.segmentedTechnologies = new Array(ctx.config.categories.length);

    for (let category = 0; category < ctx.config.categories.length; category++) {
        ctx.segmentedTechnologies [category] = new Array(ctx.config.lifecycle.length);
        for (let lifecycle = 0; lifecycle < ctx.config.lifecycle.length; lifecycle++) {
            ctx.segmentedTechnologies[category][lifecycle] = [];
        }
    }
    for (let i = 0; i < ctx.data.technologies.length; i++) {
        const tech = ctx.data.technologies[i];
        const techCategoryIdx = ctx.config.categories.map(q => q.name).indexOf(tech.category);
        const techLifecycleIdx = ctx.config.lifecycle.map(r => r.name).indexOf(tech.lifecycle);

        if (techLifecycleIdx === -1) continue; // not in circle
        ctx.segmentedTechnologies [techCategoryIdx][techLifecycleIdx].push(tech);
    }
}

function positionTechnologyLegend(ctx, categoryIdx, lifecycleIdx, technologyIdx) {
    const lifecycleCfg = ctx.config.lifecycle[lifecycleIdx];
    const dx = lifecycleCfg.legendInFirstColumn ? 0 : ctx.legendColWidth;
    let dy = (technologyIdx === -1 ? -18 : technologyIdx * 18);

    // move down for all previous lifecycle phases
    if (lifecycleCfg.legendInFirstColumn) {
        for (let previousLifecycleIdx = 0; previousLifecycleIdx < lifecycleIdx; previousLifecycleIdx++) {
            const wasInFirstCol = ctx.config.lifecycle[previousLifecycleIdx].legendInFirstColumn;
            if (wasInFirstCol) {
                dy = dy + 40 + ctx.segmentedTechnologies[categoryIdx][previousLifecycleIdx].length * 14;
            }
        }
    }

    return translate(
        ctx.legendOffset[categoryIdx].x + dx,
        ctx.legendOffset[categoryIdx].y + dy
    );
}

function showBubble(technology) {
    const tooltip = d3.select("#bubble text")
        .text(technology.name);
    const bbox = tooltip.node().getBBox();
    d3.select("#bubble")
        .attr("transform", translate(technology.x - bbox.width / 2, technology.y - 16))
        .style("opacity", 0.8);
    d3.select("#bubble rect")
        .attr("x", -5)
        .attr("y", -bbox.height)
        .attr("width", bbox.width + 10)
        .attr("height", bbox.height + 4);
    d3.select("#bubble path")
        .attr("transform", translate(bbox.width / 2 - 5, 3));
}

function hideBubble() {
    d3.select("#bubble")
        .attr("transform", translate(0, 0))
        .style("opacity", 0);
}

function polar(cartesian) {
    const x = cartesian.x;
    const y = cartesian.y;
    return {
        t: Math.atan2(y, x),
        r: Math.sqrt(x * x + y * y)
    }
}

function cartesian(polar) {
    return {
        x: polar.r * Math.cos(polar.t),
        y: polar.r * Math.sin(polar.t)
    }
}

function bounded_interval(value, min, max) {
    const low = Math.min(min, max);
    const high = Math.max(min, max);
    return Math.min(Math.max(value, low), high);
}

function bounded_ring(polar, r_min, r_max) {
    return {
        t: polar.t,
        r: bounded_interval(polar.r, r_min, r_max)
    }
}

function bounded_box(point, min, max) {
    return {
        x: bounded_interval(point.x, min.x, max.x),
        y: bounded_interval(point.y, min.y, max.y)
    }
}

// custom random number generator, to make random sequence reproducible
function random() {
    const x = Math.sin(randomSeed++) * 10000;
    return x - Math.floor(x);
}

function random_between(min, max) {
    return min + random() * (max - min);
}

function normal_between(min, max) {
    return min + (random() + random()) * 0.5 * (max - min);
}

function translate(x, y) {
    return "translate(" + x + "," + y + ")";
}