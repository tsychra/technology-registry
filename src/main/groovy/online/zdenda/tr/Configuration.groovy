package online.zdenda.tr

class Configuration {

    Data data
    Output output

    class Data {
        String provider
        String filePath
        String parentPage
        String[] categoryTags
        String[] lifecycleTags
        String titleTemplate
        boolean internalLinks
        String internalLinkPrefix
        String externalLinkPredecessor
    }

    class Output {
        String builder
        String dir
        String svgConfigPath
        String headSnippetPath
        String descriptionSnippetPath
    }
}
