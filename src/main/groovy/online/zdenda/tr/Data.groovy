package online.zdenda.tr

import groovy.transform.ToString

/**
 * Represents data for technology registry. These are typically serialized into JSON.
 */
@ToString
class Data {

    String title
    List<Technology> technologies

    static class Technology {
        String name
        String category
        String lifecycle
        String link
        int moved

        boolean hasAllRequiredInfo() {
            return name != null && category != null && lifecycle != null && link != null
        }

        @Override
        String toString() {
            return "[name=$name,category=$category,lifecycle=$lifecycle]"
        }
    }
}
