package online.zdenda.tr.provider

import online.zdenda.tr.BuildRegistry
import online.zdenda.tr.Configuration
import online.zdenda.tr.Data

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.zip.ZipFile

import static online.zdenda.tr.BuildRegistry.exit

/**
 * Data provider from Confluence exported ZIP archive.
 * It extracts {@link #XML_FILE} and uses {@link ConfluenceSpaceParser) for space-specific data retrieval.
 * If you have your own specific Confluence structure, you can re-use this provider and only pass custom parser.
 */
class ConfluenceZipProvider implements DataProvider {

    private static XML_FILE = "entities.xml"
    private final ConfluenceSpaceParser spaceParser

    ConfluenceZipProvider(ConfluenceSpaceParser spaceParser) {
        this.spaceParser = spaceParser
    }

    Data provide(Configuration cfg) {
        println("- Providing data from Confluence ZIP archive")
        def exportedWikiSpace = getExportedWikiSpace(cfg)
        def entitiesXml = getEntitiesXml(exportedWikiSpace)
        def data = spaceParser.parse(cfg, entitiesXml)
        println("  - Successfully provided data")
        return data
    }

    private File getExportedWikiSpace(Configuration cfg) {
        File srcZip = getSourceZipArchive(cfg)
        def targetFile = new File(BuildRegistry.TMP_DIR, "Confluence-export.zip")
        Files.copy(srcZip.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
        return targetFile
    }

    private File getSourceZipArchive(Configuration cfg) {
        if (cfg.data.filePath == null) {
            exit("Path to ZIP archive (provider.filePath) is missing in configuration")
        }
        def srcZip = new File(cfg.data.filePath)
        if (!srcZip.exists()) {
            exit("Path to ZIP archive (provider.filePath) is missing in configuration")
        }
        return srcZip
    }

    private String getEntitiesXml(File wikiContentsFile) {
        println("  - Unzipping wiki contents from $wikiContentsFile.canonicalPath")

        def zipFile = new ZipFile(wikiContentsFile)
        def entitiesEntry = zipFile.getEntry(XML_FILE)
        if (entitiesEntry == null) {
            exit("ZIP archive does not contain $XML_FILE file")
        }
        def entitiesContents = zipFile.getInputStream(entitiesEntry).withCloseable {
            it.getText(BuildRegistry.ENCODING)
        }
        zipFile.close()

        return entitiesContents
    }


}
