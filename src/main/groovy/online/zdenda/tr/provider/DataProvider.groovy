package online.zdenda.tr.provider

import online.zdenda.tr.Data
import online.zdenda.tr.Configuration

/**
 * Data provider is responsible for providing the data of technologies registry.
 */
interface DataProvider {

    /**
     * Provides data of technologies registry.
     *
     * @param cfg configuration
     * @return data of registry
     */
    Data provide(Configuration cfg)
}