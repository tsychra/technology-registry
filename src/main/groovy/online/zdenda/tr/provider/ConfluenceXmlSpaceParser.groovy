package online.zdenda.tr.provider

import groovy.util.slurpersupport.GPathResult
import online.zdenda.tr.Configuration
import online.zdenda.tr.Data

import static online.zdenda.tr.BuildRegistry.exit

/**
 * Parser for exported Confluence space in XML format (e.g. entities.xml).
 *
 * It expects that all technologies are sub-pages of configured parent page.
 * Category, lifecycle, promoted/demoted are expected as tags of the pages (labellings in entities.xml).
 */
class ConfluenceXmlSpaceParser implements ConfluenceSpaceParser {

    Data parse(Configuration cfg, String spaceContents) {
        println("  - Parsing Confluence space from XML")

        def parseContext = new ParseContext(
                entitiesRoot: new XmlSlurper().parseText(spaceContents),
                cfg: cfg
        )

        collectTechnologyPages(parseContext)
        collectLabellingIdToLabelName(parseContext)
        collectBodyContentIdToContent(parseContext)
        return buildData(parseContext)
    }

    private collectTechnologyPages(ParseContext ctx) {
        def allPages = ctx.entitiesRoot.depthFirst().findAll {
            hasNameClassPackage(it, "object", "Page", "com.atlassian.confluence.pages")
        }
        def allPagesById = allPages.collectEntries { [(it.id.text()): it] }

        def technologiesRegistryPage = getLatestTechnologiesRegistryPage(ctx, allPages)
        if (technologiesRegistryPage == null) exit("Page with name '$ctx.cfg.data.parentPage' is missing in exported Confluence space")
        ctx.technologyPages = technologiesRegistryPage.collection
                .find { it.@name == "children" }
                .element
                .collect { it.id.text() } // concrete technology page IDs
                .collect { pageId -> allPagesById.get(pageId) }
    }

    private collectLabellingIdToLabelName(ParseContext ctx) {
        def labels = ctx.entitiesRoot.depthFirst().findAll {
            hasNameClassPackage(it, "object", "Label", "com.atlassian.confluence.labels")
        }
        def labelIdToLabelName = labels.collectEntries {
            [(it.id.text()): getProp(it, "name")]
        }

        def labellings = ctx.entitiesRoot.depthFirst().findAll {
            hasNameClassPackage(it, "object", "Labelling", "com.atlassian.confluence.labels")
        }
        def labellingIdToLabelId = labellings.collectEntries {
            [(it.id.text()): it.property.find { it.@name == "label" }?.id?.text()]
        }

        ctx.labellingIdToLabel = (Map<String, String>) labellingIdToLabelId.collectEntries {
            [(it.key): labelIdToLabelName[it.value]]
        }
    }

    private collectBodyContentIdToContent(ParseContext ctx) {
        def bodyContents = ctx.entitiesRoot.depthFirst().findAll {
            hasNameClassPackage(it, "object", "BodyContent", "com.atlassian.confluence.core")
        }
        ctx.bodyContentIdToContent = (Map<String, String>) bodyContents.collectEntries {
            [(it.id.text()): getProp(it, "body")]
        }
    }

    private Object getLatestTechnologiesRegistryPage(ParseContext ctx, List allPages) {
        def allTechnologiesRegistryPages = allPages.findAll { ctx.cfg.data.parentPage == getProp(it, "title") }
        return allTechnologiesRegistryPages.max { page1, page2 ->
            int page1Version = Integer.valueOf(getProp(page1, "version"))
            int page2Version = Integer.valueOf(getProp(page2, "version"))
            return page1Version - page2Version
        }
    }

    private Data buildData(ParseContext ctx) {
        def technologies = new ArrayList<Data.Technology>()

        ctx.technologyPages.each { page ->
            def pageId = (String) page.id.text()
            def pageName = getProp(page, "title")
            def link
            if (ctx.cfg.data.internalLinks) {
                link = ctx.cfg.data.internalLinkPrefix
                if (!ctx.cfg.data.internalLinkPrefix.endsWith("/")) link += "/"
                link += pageId
            } else {
                def bodyContent = getPageBodyContent(page, ctx)
                link = parseExternalLink(ctx, bodyContent, pageName)
            }

            String category = null
            String lifecycle = null
            def moved = 0

            page.collection
                    .findAll { hasName(it, "labellings") && hasClass(it, "java.util.Collection") }
                    .element
                    .collect { ctx.labellingIdToLabel[(String) it.id.text()] }
                    .each { labelName ->
                if (category == null) category = ctx.cfg.data.categoryTags.find { it.toString() == labelName }
                if (lifecycle == null) lifecycle = ctx.cfg.data.lifecycleTags.find { it.toString() == labelName }
                if (labelName == "promoted") moved = 1
                if (labelName == "demoted") moved = -1
            }
            def technology = new Data.Technology(
                    name: pageName,
                    link: link,
                    category: category,
                    lifecycle: lifecycle,
                    moved: moved)

            if (technology.hasAllRequiredInfo()) {
                technologies.add(technology)
            } else {
                println("! Technology does not have all required info and will not be included in visualization: $technology")
            }
        }

        return new Data(title: resolveTitle(ctx.cfg.data.titleTemplate), technologies: technologies)
    }

    private String resolveTitle(String titleTemplate) {
        if (titleTemplate == null) return "Default Title (was not configured)"
        def now = Calendar.instance
        int quarter = (now.get(Calendar.MONTH) / 3) + 1

        String title = titleTemplate
        title = title.replace("{day}", now.get(Calendar.DAY_OF_MONTH).toString())
        title = title.replace("{month}", (now.get(Calendar.MONTH) + 1).toString())
        title = title.replace("{year}", now.get(Calendar.YEAR).toString())
        title = title.replace("{quarter}", "Q$quarter")
        return title
    }

    private String getPageBodyContent(Object page, ParseContext ctx) {
        def bodyContentId = (String) page.collection.findAll {
            hasName(it, "bodyContents") && hasClass(it, "java.util.Collection")
        }.element.id.text()
        return ctx.bodyContentIdToContent[bodyContentId]
    }

    private String parseExternalLink(ParseContext ctx, String bodyContent, String pageName) {
        if (bodyContent == null) return ""
        def predecessor = ctx.cfg.data.externalLinkPredecessor

        def predecessorIdx = bodyContent.indexOf(predecessor)
        if (predecessorIdx == -1) {
            println("! Predecessor '$predecessor' not found in page content of $pageName, link will be empty")
            return ""
        }

        def hrefIdentifier = "<a href=\""
        def hrefIdx = bodyContent.indexOf(hrefIdentifier, predecessorIdx)
        if (hrefIdx == -1) return ""

        def hrefEndIdentifier = "\">"
        def hrefEndIdx = bodyContent.indexOf(hrefEndIdentifier, hrefIdx)
        if (hrefEndIdx == -1) return ""

        return bodyContent.substring(hrefIdx + hrefIdentifier.length(), hrefEndIdx)
    }

    private String getProp(Object node, String name) {
        return node.property.find { it.@name == name }.text()
    }

    private boolean hasName(Object node, String name) {
        return node.@name == name
    }

    private boolean hasClass(Object node, String clazz) {
        return node.@class.toString() == clazz
    }

    private boolean hasPackage(Object node, String pkg) {
        return node.@package == pkg
    }

    private boolean hasNameClassPackage(Object node, String name, String clazz, String pkg) {
        return node.name() == name && hasClass(node, clazz) && hasPackage(node, pkg)
    }

    // Easier passing of parameters during parsing
    private static class ParseContext {
        Configuration cfg
        GPathResult entitiesRoot
        Collection<Object> technologyPages
        Map<String, String> bodyContentIdToContent
        Map<String, String> labellingIdToLabel
    }
}
